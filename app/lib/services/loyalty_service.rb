class Services::LoyaltyService
  def self.call(params)
    service = new(params)
    service.run
  end

  # TODO: Would be interesting if the returning struct could be customizable
  # with additional
  def run
    begin
      call
    rescue => exception
      OpenStruct.new(success?: false, error: exception)
    else
      OpenStruct.new(success?: true, error: nil)
    end
  end
end
