require 'rails_helper'

needs 'lib/services'
require 'rewards/free_coffee'

RSpec.describe Services::Rewards::FreeCoffee do
  let(:service) { described_class.new }

  it 'initializes the service' do
    expect { service }.to_not raise_error
  end

  describe 'candidate_users' do
    let(:scores_for_users_with_rewards) { create_list :score, 2 }
    let(:users_with_awards) { scores_for_users_with_rewards.map(&:user) }
    let(:no_reward_score) { create :score }

    before do
      create :point, num_points: 100, score: scores_for_users_with_rewards[0]
      create :point, num_points: 50, score: scores_for_users_with_rewards[1]
      create :point, num_points: 50, score: scores_for_users_with_rewards[1]

      Timecop.freeze(2.month.ago) do
        create :point, num_points: 100, score: no_reward_score
      end
    end

    it 'returns users that match the reward rule' do
      expect(service.candidate_users).to match_array users_with_awards
    end
  end

  describe 'reward' do
    let(:reward) { create :reward, ref: 'free-monthly-reward' }
    before do
      reward
    end

    it 'returns the reward record by reference' do
      expect(service.reward).to eq reward
    end
  end
end
