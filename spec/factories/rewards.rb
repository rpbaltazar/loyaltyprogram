FactoryBot.define do
  factory :reward do
    ref { Faker::Name.name }
    description { Faker::Internet.email }
  end
end
