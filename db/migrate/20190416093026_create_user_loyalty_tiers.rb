class CreateUserLoyaltyTiers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_loyalty_tiers do |t|
      t.bigint :user_id
      t.references :loyalty_tier
      t.datetime :assigned_at
      t.datetime :expired_at
      t.timestamps
    end

    add_index :user_loyalty_tiers, :user_id, where: "expired_at IS NULL", unique: true
  end
end
