class User < ApplicationRecord
  has_many :money_transactions
  has_one :score
  has_many :user_loyalty_tiers
  has_many :loyalty_tiers, through: :user_loyalty_tiers, class_name: 'LoyaltyTier'
  has_one :loyalty_tier, -> { where(expired_at: nil) }, class_name: 'UserLoyaltyTier'

  validates_presence_of :email
  validates_uniqueness_of :email

  before_validation :downcase_email

  def expire_current_tier
    loyalty_tier.expire
  end

  def unexpired_points
    return 0 unless score

    score.unexpired_points
  end

  private

  def downcase_email
    self.email = email.downcase if attribute_present? 'email'
  end
end
