require 'rails_helper'

RSpec.describe Score, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:points) }
    it { is_expected.to belong_to(:user) }
  end

  describe 'points' do
    let(:user) { create :user }
    let(:score) { user.score }

    before do
      Timecop.freeze(2.year.ago) do
        Services::PointsAdder.call(num_points: 100, user_id: user.id)
      end
      Timecop.freeze(1.year.ago) do
        Services::PointsAdder.call(num_points: 200, user_id: user.id)
      end
      Timecop.freeze(1.year.ago + 1.day) do
        Services::PointsAdder.call(num_points: 300, user_id: user.id)
      end
      Services::PointsAdder.call(num_points: 400, user_id: user.id)
    end

    describe 'unexpired points' do
      it 'returns the sum of points that were added in the past year' do
        expect(score.unexpired_points).to eq 700
      end
    end

    describe 'all points' do
      it 'returns the sum of all points ever assigned' do
        expect(score.all_points).to eq 1000
      end
    end
  end
end
