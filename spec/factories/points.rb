FactoryBot.define do
  factory :point do
    association :score
    num_points { Faker::Number.number 2 }
  end
end
