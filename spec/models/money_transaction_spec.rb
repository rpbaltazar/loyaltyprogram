require 'rails_helper'

RSpec.describe MoneyTransaction, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_presence_of(:transaction_date) }
    it { is_expected.to validate_presence_of(:currency) }
    it { is_expected.to validate_presence_of(:user_id) }
  end
end
