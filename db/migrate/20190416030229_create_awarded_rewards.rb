class CreateAwardedRewards < ActiveRecord::Migration[5.2]
  def change
    create_table :awarded_rewards do |t|
      t.references :user
      t.references :reward
      t.timestamps
    end
  end
end
