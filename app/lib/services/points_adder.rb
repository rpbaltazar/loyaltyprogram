# Points Adder class will help us adding points to a specific user id
class Services::PointsAdder < Services::LoyaltyService
  def initialize(num_points:, user_id:)
    @score = Score.find_or_create_by(user_id: user_id)
    @num_points = num_points
  end

  def call
    Point.create!(score: @score, num_points: @num_points)
  end
end
