# Services::PointsConverter.call(user_id: user.id)
# Responsible for fetching the unprocessed transactions,
# calculate points to add,
# add the points to the user,
# mark the transactions as processed,
# create a virtual transaction with the remaining,
# Check for loyalty state
# changes and update the loyalty status if needed

class Services::PointConverter < Services::LoyaltyService
  def initialize(user_id:)
    @user_id = user_id
  end

  private

  def call
    points_to_add = calculate_points_to_add(unprocessed_transactions)
    return unless points_to_add.positive?

    result = Services::PointsAdder.call(num_points: points_to_add, user_id: @user_id)
    raise result.error unless result.success?

    process_transactions(unprocessed_transactions, points_to_add)
    result = Services::LoyaltyChecker.call(user_id: @user_id)
    raise result.error unless result.success?
  end

  def calculate_points_to_add(transaction_list)
    total_spending_pending = transaction_list.sum(&:amount)
    (total_spending_pending / Loyaltyprogram::MIN_SPENDING).to_i * Loyaltyprogram::POINTS_AWARD
  end

  def process_transactions(transaction_list, added_points)
    money_to_process = added_points * Loyaltyprogram::POINTS_AWARD
    transaction_list.each do |transaction|
      break if money_to_process.zero?

      money_to_process = process_transaction(transaction, money_to_process)
    end
  end

  def process_transaction(transaction, money_to_process)
    if transaction.amount <= money_to_process
      money_to_process -= transaction.amount
    else
      remaining = transaction.amount - money_to_process
      money_to_process -= remaining
      create_virtual_transaction(remaining)
    end
    transaction.processed = true
    transaction.save!
    money_to_process
  end

  def unprocessed_transactions
    @unprocessed_transactions ||= MoneyTransaction.unprocessed
  end

  def create_virtual_transaction(remaining)
    remaining_transaction = MoneyTransaction.new(
      amount: remaining,
      currency: 'USD',
      user_id: @user_id,
      transaction_date: Date.today,
      processed: false,
      virtual: true
    )
    remaining_transaction.save!
  end
end
