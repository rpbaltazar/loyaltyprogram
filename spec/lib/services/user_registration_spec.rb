require 'rails_helper'

RSpec.describe Services::UserRegistration do
  before do
    create :loyalty_tier, name: 'Standard'
  end

  it 'initializes the service' do
    user = User.new(email: 'test@something.com')
    expect { described_class.call(user: user) }.to_not raise_error
  end

  it 'raises error when missing user data' do
    expect { described_class.call }.to raise_error(ArgumentError, %r{given 0, expected 1})
  end

  describe 'call' do
    let(:service_result) { described_class.call params }

    describe 'when the data is valid' do
      let(:params) { { user: User.new(email: 'test@something.com') } }
      let(:user) { User.last }

      it 'returns a result object with success? true' do
        expect(service_result.success?).to eq true
      end

      it 'creates a user with the standard loyalty tier' do
        expect { service_result }.to change { User.count }.by(1)
        expect(user.email).to eq 'test@something.com'
      end

      it 'associates the standard loyalty tier to the user' do
        service_result
        expect(user.loyalty_tiers.count).to eq 1
        expect(user.loyalty_tier.name).to eq 'Standard'
      end
    end

    describe 'when the tier is passed along' do
      let(:params) do
        {
          user: User.new(email: 'test@something.com'),
          tier: create(:loyalty_tier, name: 'Amazing')
        }
      end
      let(:user) { User.last }

      it 'returns a result object with success? true' do
        expect(service_result.success?).to eq true
      end

      it 'associates the standard loyalty tier to the user' do
        service_result
        expect(user.loyalty_tiers.count).to eq 1
        expect(user.loyalty_tier.name).to eq 'Amazing'
      end
    end

    describe 'when there are errors registering the user' do
      let(:params) do
        {
          user: User.new(email: 'tony@mailinator.com'),
          tier: create(:loyalty_tier, name: 'Amazing')
        }
      end
      let(:user) { User.last }

      before do
        create :user, email: 'Tony@mailinator.com'
      end

      it 'returns a result object with success? false' do
        expect(service_result.success?).to eq false
      end

      it 'does not save the user' do
        expect { service_result }.to_not(change { User.count })
      end

      it 'returns the error in the result' do
        expect(service_result.error.message).to eq 'Validation failed: Email has already been taken'
      end
    end
  end
end
