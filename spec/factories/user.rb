FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    birthday { Faker::Date.birthday(18, 65) }
    phone_number { Faker::PhoneNumber.phone_number }
  end
end
