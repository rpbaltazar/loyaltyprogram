# TransactionTracker is be responsible for tracking a transaction done by
# a user. This implies updating its total points
class Services::TransactionTracker
  def initialize(transaction_data)
    @user_id = transaction_data['user_id']
    @transaction = initialize_transaction(transaction_data)
  end

  def register
    # TODO: Wrap this around a transaction so we can rollback
    @transaction.save!
    Services::PointConverter.call(user_id: @user_id)
  end

  private

  def initialize_transaction(transaction_data)
    transaction_data[:processed] = false
    transaction_data[:virtual] = false
    MoneyTransaction.new(transaction_data)
  end
end
