class MoneyTransaction < ApplicationRecord

  belongs_to :user

  validates_presence_of :amount, :currency, :transaction_date, :user_id

  scope :unprocessed, -> { where(processed: false) }
end
