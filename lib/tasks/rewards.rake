namespace :rewards do
  desc "Award a coffee if a user has accumulated 100 points in one calendar month"
  # NOTE: This is set as a rake task but ultimately we'd want to run it in a
  # scheduled task (cronjob) and in a sidekiq worker
  task free_monthly_coffe: :environment do
    FreeCoffeeWorker.perform_async
  end
end
