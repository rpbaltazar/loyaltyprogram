class Reward < ApplicationRecord
  validates_presence_of :ref
  has_many :awarded_rewards
end
