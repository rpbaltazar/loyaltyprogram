class AddVirtualToMoneyTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :money_transactions, :virtual, :boolean
  end
end
