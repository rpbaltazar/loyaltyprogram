require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:money_transactions) }
    it { is_expected.to have_many(:user_loyalty_tiers) }
    it { is_expected.to have_many(:loyalty_tiers).through(:user_loyalty_tiers) }
    it { is_expected.to have_one(:loyalty_tier) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
  end
end
