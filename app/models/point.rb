class Point < ApplicationRecord
  belongs_to :score
  has_one :user, through: :score

  validates_presence_of :score
end
