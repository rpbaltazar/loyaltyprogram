class UserLoyaltyTier < ApplicationRecord
  belongs_to :loyalty_tier
  belongs_to :user

  delegate :name, to: :loyalty_tier

  validates_uniqueness_of :user, conditions: -> { where(expired_at: nil) }

  def expire(expire_date = Time.now)
    self.expired_at = expire_date
    save
  end
end
