require 'rails_helper'

RSpec.describe Services::TransactionTracker do
  let(:other_service_result) { OpenStruct.new(success?: true, error: nil) }

  before do
    allow(Services::PointConverter).to receive(:call).and_return other_service_result
  end

  it 'initializes a transaction tracker' do
    transaction_data = {
      amount: 10,
      currency: 'USD',
      user_id: 1,
      transaction_date: Date.today
    }

    expect { described_class.new(transaction_data.as_json) }.to_not raise_error
  end

  it 'raises error when missing transaction data' do
    expect { described_class.new }.to raise_error(ArgumentError, %r{given 0, expected 1})
  end

  describe 'when the transaction data is valid' do
    let(:user) { create :user }
    let(:transaction_data) do
      {
        amount: 10,
        currency: 'USD',
        user_id: user.id,
        transaction_date: Date.today
      }
    end
    let(:transaction_tracker) { described_class.new(transaction_data.as_json) }

    it 'creates a new money transaction' do
      expect { transaction_tracker.register }.to change{ MoneyTransaction.count }.from(0).to(1)
    end

    it 'runs the point coverter service' do
      transaction_tracker.register
      expect(Services::PointConverter).to have_received(:call).with(user_id: user.id)
    end
  end
end
