class CreateMoneyTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :money_transactions do |t|
      t.float :amount
      t.datetime :transaction_date
      t.string :currency
      t.references :user
      t.boolean :processed

      t.timestamps
    end
  end
end
