FactoryBot.define do
  factory :user_loyalty_tier do
    association :user
    association :loyalty_tier
  end
end
