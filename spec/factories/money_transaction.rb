FactoryBot.define do
  factory :money_transaction do
    amount { Faker::Number.number(3) }
    transaction_date { Faker::Date.birthday(18, 65) }
    currency { 'USD' }
    association :user
    processed { false }
    virtual { false }
  end
end
