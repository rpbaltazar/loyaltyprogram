class FreeCoffeeWorker
  include Sidekiq::Worker

  def perform
    service = Services::Rewards::FreeCoffee.new
    service.candidate_users.each do |u|
      AwardedReward.create!(reward: service.reward, user: u)
    end
  end
end
