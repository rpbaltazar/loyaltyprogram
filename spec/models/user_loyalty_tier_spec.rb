require 'rails_helper'

RSpec.describe UserLoyaltyTier, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:loyalty_tier) }
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    describe 'loyalty tiers per user' do
      let(:tiers) { create_list :loyalty_tier, 2 }
      let(:user) { create :user }

      before do
        UserLoyaltyTier.create!(user: user, loyalty_tier: tiers[0])
      end

      it 'only allows one not expired' do
        expect { UserLoyaltyTier.create!(user: user, loyalty_tier: tiers[1]) }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end
end
