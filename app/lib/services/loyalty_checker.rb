class Services::LoyaltyChecker < Services::LoyaltyService
  def initialize(user_id:)
    @user_id = user_id
    # TODO: This can probably be long term cached.
    # No point in keep searching for these every time.
    @standard_tier = LoyaltyTier.find_by(name: 'Standard')
    @gold_tier = LoyaltyTier.find_by(name: 'Gold')
    @platinum_tier = LoyaltyTier.find_by(name: 'Platinum')
  end

  def call
    user = User.find_by(id: @user_id)
    tier = tier_for_points(user.unexpired_points)
    update_tier_for_user(user, tier)
  end

  private

  def tier_for_points(num_points)
    if num_points < 1000
      @standard_tier
    elsif num_points < 5000
      @gold_tier
    else
      @platinum_tier
    end
  end

  def update_tier_for_user(user, tier)
    return unless user.loyalty_tier != tier

    user.exire_current_tier
    user.create_loyalty_tier!(loyalty_tier_id: tier.id)
  end
end
