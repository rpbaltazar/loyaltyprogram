require 'spec_helper_little'
require 'rails_helper'

needs 'lib/services'
require 'rewards/free_coffee'

RSpec.describe FreeCoffeeWorker, type: :worker do
  let(:users_for_reward) { create_list :user, 2 }
  let(:other_users) { create_list :user, 2 }
  let(:reward) { create :reward }
  let(:fake_reward_service) do
    instance_double(Services::Rewards::FreeCoffee,
                    candidate_users: users_for_reward,
                    reward: reward)
  end

  before do
    allow(Services::Rewards::FreeCoffee).to receive(:new).and_return fake_reward_service
    FreeCoffeeWorker.new.perform
  end

  it 'creates an award for each user that deserves it' do
    expect(AwardedReward.count).to eq 2
    expect(AwardedReward.all.map(&:user)).to match_array users_for_reward
  end
end
