# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Ruby version

- 2.6.2

## System dependencies

- Graphviz - for generating the ERD on schema changes
  - `brew install graphviz`

- PostgreSQL - as it is the database manager configured
  - `brew install postgresql`

## Database configuration

The connection to the database is defined on config/database.yml file.
The default assumption is that there are no special variables to be defined to access to your local database. In case there are, these should be set using environment variables. There are 5 environment variables that the config/database.yml is looking for and using:

- RAILS_MAX_THREADS - to configure the number of connections to the database (aka pool size). It defaults to 5
- POSTGRES_DB - database name. It defaults to `loyalty_test`
- POSTGRES_USER - user with permissions to access the database. It defaults to `` (empty)
- POSTGRES_USER - user's password to access the database. It defaults to `` (empty)
- POSTGRES_HOST - database host. It defaults to `` (empty)

## Database initialization - TODO

There is a seed that can be ran and will populate the database with the skeleton data set for the application.
It populates with the possible rewards and available tiers

## How to run the test suite

CI integration has been done with Gitlab's CI, but in case you want/need to run
the test suite in your local machine, you can do so by running:

`bundle exec rspec`

or if you want to run a specific test

`bundle exec rspec spec/models/awarded_reward_spec.rb`

## Services

We are using Sidekiq workers for our runners.

## Deployment instructions - TODO
