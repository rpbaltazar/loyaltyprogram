require 'rails_helper'

RSpec.describe Point, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :score }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:score) }
    it { is_expected.to have_one(:user).through(:score) }
  end
end
