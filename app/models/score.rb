class Score < ApplicationRecord
  has_many :points
  belongs_to :user

  def unexpired_points
    points.where('points.created_at > ?', 1.year.ago).sum(:num_points)
  end

  def all_points
    points.sum(:num_points)
  end
end
