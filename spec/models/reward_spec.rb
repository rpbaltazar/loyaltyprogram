require 'rails_helper'

RSpec.describe Reward, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:awarded_rewards) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :ref }
  end
end
