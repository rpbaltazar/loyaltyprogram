require 'rails_helper'

RSpec.describe LoyaltyTier, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:user_loyalty_tiers) }
  end

  describe 'validations' do
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end
