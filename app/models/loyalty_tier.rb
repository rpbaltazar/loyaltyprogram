class LoyaltyTier < ApplicationRecord
  has_many :user_loyalty_tiers

  validates_uniqueness_of :name
end
