class Services::UserRegistration < Services::LoyaltyService
  def initialize(params)
    @user = params[:user]
    @tier = params[:tier] || LoyaltyTier.find_by(name: 'Standard')
  end

  private

  def call
    assign_tier_to_user
  end

  def assign_tier_to_user
    @user.save!
    @user.create_loyalty_tier!(loyalty_tier_id: @tier.id)
  end
end
