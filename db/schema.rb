# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_16_093026) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "awarded_rewards", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reward_id"], name: "index_awarded_rewards_on_reward_id"
    t.index ["user_id"], name: "index_awarded_rewards_on_user_id"
  end

  create_table "loyalty_tiers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "money_transactions", force: :cascade do |t|
    t.float "amount"
    t.datetime "transaction_date"
    t.string "currency"
    t.bigint "user_id"
    t.boolean "processed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "virtual"
    t.index ["user_id"], name: "index_money_transactions_on_user_id"
  end

  create_table "points", force: :cascade do |t|
    t.integer "num_points"
    t.bigint "score_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["score_id"], name: "index_points_on_score_id"
  end

  create_table "rewards", force: :cascade do |t|
    t.string "ref"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scores", force: :cascade do |t|
    t.bigint "user_id"
    t.index ["user_id"], name: "index_scores_on_user_id"
  end

  create_table "user_loyalty_tiers", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "loyalty_tier_id"
    t.datetime "assigned_at"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loyalty_tier_id"], name: "index_user_loyalty_tiers_on_loyalty_tier_id"
    t.index ["user_id"], name: "index_user_loyalty_tiers_on_user_id", unique: true, where: "(expired_at IS NULL)"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.date "birthday"
    t.string "email"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
