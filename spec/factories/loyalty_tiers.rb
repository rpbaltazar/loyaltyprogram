FactoryBot.define do
  factory :loyalty_tier do
    name { Faker::Name.name }
  end
end
