class Services::Rewards::FreeCoffee
  # TODO: improvement - initialize service with a specific month-year so we
  # can rerun the award assigning. if for some reason while generating the
  # awards we crash, we could reuse this service to find who deserves an
  # award that has not yet been given
  def initialize
    @ref = 'free-monthly-reward'
    @min_points = 100
    @period = 1.month.ago
  end

  def candidate_users
    User.joins(:score).where('scores.id in (?)',
                             Point.select(:score_id)
                                  .where('created_at > ?', @period)
                                  .having('sum(num_points) >= ?', @min_points)
                                  .group(:score_id))
  end

  def reward
    Reward.find_by(ref: @ref)
  end
end
