require 'rails_helper'

RSpec.describe Services::PointConverter do
  it 'initializes the point converter' do
    expect { described_class.call(user_id: 1) }.to_not raise_error
  end

  it 'raises error when missing transaction data' do
    expect { described_class.call }.to raise_error(ArgumentError, %r{given 0, expected 1})
  end

  describe 'when the user_id is passed' do
    let(:user) { create :user }
    let(:result) { described_class.call(user_id: user.id)}
    let(:transactions) { build_list :money_transaction, 2 }
    let(:other_service_result) { OpenStruct.new(success?: true, error: nil) }

    before do
      allow(MoneyTransaction).to receive(:unprocessed).and_return transactions
      allow(Services::PointsAdder).to receive(:call).and_return other_service_result
      allow(Services::LoyaltyChecker).to receive(:call).and_return other_service_result
    end

    describe 'when there are no unprocessed transactions' do
      let(:transactions) { [] }

      it 'runs the operation successfully' do
        expect(result.success?).to eq true
      end

      it 'does not add any points' do
        expect { result }.to_not(change { Point.count })
      end
    end

    describe 'when there are unprocessed transactions' do
      describe 'when there is not enough spending for points to be added' do
        let(:transactions) do
          [create(:money_transaction, amount: 10, user_id: user.id)]
        end

        it 'runs the operation successfully' do
          expect(result.success?).to eq true
        end

        it 'does not add any points' do
          result
          expect(Services::PointsAdder).to_not have_received(:call)
        end
      end

      describe 'when there are enough spending for points to be added' do
        let(:transactions) do
          [create(:money_transaction, amount: 10, user_id: user.id),
           create(:money_transaction, amount: 90, user_id: user.id)]
        end

        it 'runs the operation successfully' do
          expect(result.success?).to eq true
        end

        it 'calls the points adder with number of points and user' do
          result
          expect(Services::PointsAdder).to have_received(:call)
            .with(num_points: 10, user_id: user.id)
        end

        it 'processes the transactions' do
          result
          expect(transactions[0].reload.processed).to eq true
          expect(transactions[1].reload.processed).to eq true
        end

        it 'runs a loyalty check' do
          result
          expect(Services::LoyaltyChecker).to have_received(:call)
            .with(user_id: user.id)
        end
      end

      describe 'when the spending is not exact number of points' do
        let(:transactions) do
          [create(:money_transaction, amount: 10, user_id: user.id),
           create(:money_transaction, amount: 110, user_id: user.id)]
        end

        it 'calls the points adder with number of points and user' do
          result
          expect(Services::PointsAdder).to have_received(:call)
            .with(num_points: 10, user_id: user.id)
        end

        it 'processes the transactions' do
          result
          expect(transactions[0].reload.processed).to eq true
          expect(transactions[1].reload.processed).to eq true
        end

        it 'creates a virtual transaction to hold the remaining' do
          result
          remaining = MoneyTransaction.last
          expect(remaining.amount).to eq 20
          expect(remaining.processed).to eq false
          expect(remaining.virtual).to eq true
        end
      end
    end
  end
end
